package com.weproov.checkpict.exceptions;

import com.weproov.checkpict.constants.ExceptionMessages;

public class ResourceNotFoundException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8673625176481258817L;

	public ResourceNotFoundException() {
		super(ExceptionMessages.RESOURCE_NOT_FOUND) ;
	}
	
	public ResourceNotFoundException(String message) {
		super(message) ;
	}
}
