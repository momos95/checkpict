package com.weproov.checkpict.exceptions;

public class BusinessValidationException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 855515498414531198L;
	
	public BusinessValidationException() {
		super("Erreur de validation métier") ;
	}
	
	public BusinessValidationException(String message) {
		super(message) ;
	}
	
}
