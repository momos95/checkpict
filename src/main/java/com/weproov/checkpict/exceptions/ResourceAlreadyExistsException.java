package com.weproov.checkpict.exceptions;

import com.weproov.checkpict.constants.ExceptionMessages;

public class ResourceAlreadyExistsException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 855515498414531198L;
	
	public ResourceAlreadyExistsException() {
		super(ExceptionMessages.RESOURCE_ALREADY_EXISTS) ;
	}
	
	public ResourceAlreadyExistsException(String message) {
		super(message) ;
	}
	
}
