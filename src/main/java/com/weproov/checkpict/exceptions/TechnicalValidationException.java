package com.weproov.checkpict.exceptions;

public class TechnicalValidationException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 855515498414531198L;
	
	public TechnicalValidationException() {
		super("Erreur technique") ;
	}
	
	public TechnicalValidationException(String message) {
		super(message) ;
	}
	
}
