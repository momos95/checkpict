package com.weproov.checkpict.exceptions;

import com.weproov.checkpict.constants.ExceptionMessages;

public class UserBadCredentialsException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8673625176481258817L;

	public UserBadCredentialsException() {
		super(ExceptionMessages.RESOURCE_NOT_FOUND) ;
	}
	
	public UserBadCredentialsException(String message) {
		super(message) ;
	}
}
