package com.weproov.checkpict.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.weproov.checkpict.entites.Photo;

public interface PhotoRepository extends JpaRepository<Photo, Long> {
	public Page<Photo> findByInspectionId(Long inspectionID, Pageable pageable);
}
