package com.weproov.checkpict.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.weproov.checkpict.entites.AnnotationAdditionalPicture;

public interface AnnotationAdditionalPictureRepository extends JpaRepository<AnnotationAdditionalPicture, Long> {
	public Page<AnnotationAdditionalPicture> findByAnnotationId(Long annotationID, Pageable pageable);
}
