package com.weproov.checkpict.repositories;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.weproov.checkpict.entites.User;
import com.weproov.checkpict.enums.Role;

public interface UserRepository extends JpaRepository<User, Long>{
	boolean existsByUsername(String username) ;
	Page<User> findByRole(Role role, Pageable pageable) ;
	Optional<User> findByUsername(String username);
}
