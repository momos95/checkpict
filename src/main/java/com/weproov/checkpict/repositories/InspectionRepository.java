package com.weproov.checkpict.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.weproov.checkpict.entites.Inspection;

public interface InspectionRepository extends JpaRepository<Inspection, Long> {
	Page<Inspection> findByOwnerId(Long ownerID, Pageable pageable);

}
