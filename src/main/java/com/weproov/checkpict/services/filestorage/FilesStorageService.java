package com.weproov.checkpict.services.filestorage;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponents;

import com.weproov.checkpict.config.SecurityContext;

@Service
public abstract class FilesStorageService implements IFilesStorageService {

    @Autowired
    private SecurityContext securityContext;

    private Path root;

    private String path;

    Logger logger = LoggerFactory.getLogger(FilesStorageService.class);

    protected FilesStorageService(String path) {
        this.path = path;
        this.root = Paths.get("src/main/uploads/" + path);
    }


    @Override
    public void init() {
        try {
            if (!Files.exists(root)) Files.createDirectories(root);
        } catch (IOException e) {
            logger.error("Could not initialize folder for upload! " + e);
            throw new RuntimeException("Could not initialize folder for upload! " + e.getMessage());
        }
    }
    
    /*
     * Méthode à changer en adoptant une stratégie AWS ou GCP ou tout autre FileStorageService en ligne
     */

    @Override
    public String save(MultipartFile file) {
        String filename = new Date().getTime() + "-" + file.getOriginalFilename();
        try {
            if (Files.exists(this.root.resolve(filename)))
                delete(filename);
            Files.copy(file.getInputStream(), this.root.resolve(filename));
            logger.info("files copyed");
        } catch (Exception e) {
            logger.error("Could not store the file. Error: " + e);
            throw new RuntimeException("Could not store the file. Error: " + e.getMessage());
        }
        UriComponents request = securityContext.getRequestUri();
        final String url = request.toUriString();
        final String path = url + "/files/" + this.path + "/" + filename;
        return path;
    }

    @Override
    public Resource load(String filename) {
        try {
            Path file = root.resolve(filename);
            Resource resource = new UrlResource(file.toUri());
            logger.info(file.getFileName().toString());
            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                logger.error("Could not read the file! : " + filename);
                throw new RuntimeException("Could not read the file!");
            }
        } catch (MalformedURLException e) {
            logger.error("Error while loading file " + filename);
            throw new RuntimeException("Error: " + e.getMessage());
        }
    }

    @Override
    public void delete(String filename) throws IOException {
        try {
            if (filename != null) {
                final String[] filePath = filename.split("/");
                Files.deleteIfExists(this.root.resolve(filePath[filePath.length - 1]));
            }
        } catch (IOException e) {
            logger.error("Error while deleting image " + filename);
            throw e;
        }
    }

    @Override
    public void deleteAll() {
        FileSystemUtils.deleteRecursively(root.toFile());
    }

    @Override
    public Stream<Path> loadAll() {
        try {
            return Files.walk(this.root, 1).filter(path -> !path.equals(this.root)).map(this.root::relativize);
        } catch (IOException e) {
            throw new RuntimeException("Could not load the files!");
        }
    }

}
