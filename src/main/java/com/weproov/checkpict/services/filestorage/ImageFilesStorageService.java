package com.weproov.checkpict.services.filestorage;

import org.springframework.stereotype.Service;

@Service
public class ImageFilesStorageService extends FilesStorageService {
	
	private final static String path = "photos" ;
	
	public ImageFilesStorageService() {
		super(path) ;
	}	
		
}
