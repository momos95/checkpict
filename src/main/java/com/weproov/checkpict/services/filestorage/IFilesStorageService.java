package com.weproov.checkpict.services.filestorage;

import java.io.IOException;
import java.nio.file.Path;
import java.util.stream.Stream;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface IFilesStorageService {
	
	 public void init();

	  public String save(MultipartFile file);

	  public Resource load(String filename);

	  public void deleteAll();
	  
	  public void delete(String filename) throws IOException;

	  public Stream<Path> loadAll();

}
