package com.weproov.checkpict.services.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.weproov.checkpict.config.SecurityContext;
import com.weproov.checkpict.constants.ExceptionMessages;
import com.weproov.checkpict.entites.User;
import com.weproov.checkpict.enums.Role;
import com.weproov.checkpict.exceptions.ResourceAlreadyExistsException;
import com.weproov.checkpict.exceptions.ResourceNotFoundException;
import com.weproov.checkpict.repositories.UserRepository;
import com.weproov.checkpict.services.IUserService;

@Service
public class UserService implements IUserService {

	@Autowired
	private UserRepository mainRepository ;
	
    @Autowired
    private SecurityContext securityContext;
    
    @Autowired
    private PasswordEncoder encoder;
    
    private final Logger logger = LoggerFactory.getLogger(UserService.class);
	
	
	public Page<User> findAll(int page, int size, Role role){
		if(!getConnectedUser().getRole().equals(Role.ADMIN)) 
			throw new AccessDeniedException(ExceptionMessages.ACCESS_DENIED);
        return mainRepository.findByRole(role, getPageable(page, size)) ;
	}
	
	@Override
	public Page<User> findAll(int page, int size){
		if(!getConnectedUser().getRole().equals(Role.ADMIN))
			throw new AccessDeniedException(ExceptionMessages.ACCESS_DENIED);
        return mainRepository.findAll(getPageable(page, size)) ;
	}

	@Override
	public User find(Long id) {
		return mainRepository.findById(id).orElseThrow(ResourceNotFoundException::new);
	}
	
	public User find(String username) {
		return mainRepository.findByUsername(username).orElseThrow(ResourceNotFoundException::new) ;
	}



	@Override
	public User save(User entity) {
		
		if(!getConnectedUser().getRole().equals(Role.ADMIN))
			throw new AccessDeniedException(ExceptionMessages.ACCESS_DENIED);
		
		if (mainRepository.existsByUsername(entity.getUsername())) {
            throw new ResourceAlreadyExistsException("Il existe déjà un utilisateur avec l'adresse e-mail fournie") ;
        }
		entity.setPassword(encoder.encode(entity.getPassword()));
		User newUser = mainRepository.save(entity);
		return newUser ;
	}
	

	@Override
	public User update(User entity, Long id) {
		if(!entity.getId().equals(id) || !mainRepository.existsById(id))
			throw new ResourceNotFoundException() ;
		if(!getConnectedUser().getRole().equals(Role.ADMIN) 
				&& !getConnectedUser().getId().equals(id))
			throw new ResourceNotFoundException() ;
		if(entity.getPassword() == null)
			entity.setPassword(find(id).getPassword());
		else
			entity.setPassword(encoder.encode(entity.getPassword()));
		return mainRepository.save(entity);
	}



	@Override
	public boolean delete(Long id) {
		try {
			if(getConnectedUser().getRole().equals(Role.ADMIN) ) {
				mainRepository.delete(find(id));
				return true ;
			}
		}catch(Exception exception) {
			logger.error("Error - {0}", exception);
			return false ;
		}
		return false;
	}


	@PostAuthorize("(principal.username == returnObject.username)")
    public User getConnectedUser() {
        String email = securityContext.getLoggedUser();
        return mainRepository.findByUsername(email).orElseThrow(
                () -> new UsernameNotFoundException("User not found with email " + email));
    }
	
}
