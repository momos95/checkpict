package com.weproov.checkpict.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.weproov.checkpict.entites.Inspection;
import com.weproov.checkpict.entites.User;
import com.weproov.checkpict.enums.Role;
import com.weproov.checkpict.exceptions.BusinessValidationException;
import com.weproov.checkpict.exceptions.ResourceNotFoundException;
import com.weproov.checkpict.repositories.InspectionRepository;
import com.weproov.checkpict.services.IInspectionService;

@Service
public class InspectionService implements IInspectionService {

	@Autowired
	private InspectionRepository repository;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private PhotoService photoService;
	
	
	@Override
	public Page<Inspection> findAll(int page, int size) {
		User connectedUser = userService.getConnectedUser();
		return connectedUser.getRole().equals(Role.ADMIN) ?
				repository.findAll(getPageable(page, size)) :
					repository.findByOwnerId(connectedUser.getId(), null);
	}

	@Override
	public Inspection find(Long id) {
		return repository.findById(id).orElseThrow(ResourceNotFoundException::new);
	}

	@Override
	public Inspection save(Inspection entity) {
		entity.setOwner(userService.getConnectedUser());
		entity.setPhotos(null);
		entity.setTitle(entity.getTitle());
		return repository.save(entity);
	}

	@Override
	public Inspection update(Inspection entity, Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean delete(Long id) {
		User connectedUser = userService.getConnectedUser();
		Inspection inspection = find(id);
		
		// Check inspection's owner
		if(!connectedUser.getId().equals(inspection.getOwner().getId()))
			throw new BusinessValidationException("Inspection can be deleted only by owner.");
		
		// delete attached pictures before
		inspection.getPhotos()
		.parallelStream()
		.forEach(photo -> photoService.delete(photo.getId()));
		
		// delete inspection
		repository.delete(inspection);
		return true;
	}

}
