package com.weproov.checkpict.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.weproov.checkpict.entites.Annotation;
import com.weproov.checkpict.entites.Photo;
import com.weproov.checkpict.entites.User;
import com.weproov.checkpict.entites.forms.PhotoForm;
import com.weproov.checkpict.exceptions.BusinessValidationException;
import com.weproov.checkpict.exceptions.ResourceNotFoundException;
import com.weproov.checkpict.exceptions.TechnicalValidationException;
import com.weproov.checkpict.repositories.PhotoRepository;
import com.weproov.checkpict.services.IPhotoService;
import com.weproov.checkpict.services.filestorage.ImageFilesStorageService;

@Service
public class PhotoService implements IPhotoService {

	@Autowired
	private PhotoRepository photoRepository;

	@Autowired
	private InspectionService inspectionService;

	@Autowired
	private UserService userService;

	@Autowired
	private AnnotationService annotationService;

	@Autowired
	private ImageFilesStorageService fileStorage;

	@Override
	public Page<Photo> findAll(int page, int size) {
		// not in scope
		return null;
	}
	
	public Page<Photo> findAll(Long inspectionId, int page, int size) {
		return photoRepository.findByInspectionId(inspectionId, getPageable(page, size));
	}

	@Override
	public Photo find(Long id) {
		return photoRepository.findById(id).orElseThrow(ResourceNotFoundException::new);
	}

	@Override
	public Photo save(Photo entity) {
		return photoRepository.save(entity);
	}

	public Photo save(PhotoForm photoForm, MultipartFile file) {

		if(file == null)
			throw new BusinessValidationException("Attached Image is mandatory");

		Photo toSave = new Photo();
		toSave.setInspection(inspectionService.find(photoForm.getIdInspection()));
		toSave.setTitle(photoForm.getTitle());
		toSave.setUrl(fileStorage.save(file));
		final Photo entity = save(toSave);

		if(photoForm.getAnnotations() != null) {
			List<Annotation> annotations = new ArrayList<>();
			photoForm.getAnnotations()
			.stream()
			.forEach(anno -> {
				Annotation annotation = new Annotation();
				annotation.setPhoto(entity);
				annotation.setX(anno.getX());
				annotation.setY(anno.getY());
				annotation.setText(anno.getText());
				annotations.add(annotation);
			});
			toSave.setAnnotations(annotations);
		}
		return photoRepository.save(toSave);
	}

	@Override
	public Photo update(Photo entity, Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean delete(Long id) {
		User connectedUser = userService.getConnectedUser();
		Photo toDelete = find(id);

		// Check if connected user is picture's owner
		if(!connectedUser.getId().equals(toDelete.getInspection().getOwner().getId()))
			throw new BusinessValidationException("Only inspection's owner can delete attached picture");

		try {
			// deleting annotations before
			toDelete.getAnnotations()
			.parallelStream()
			.forEach(anno -> annotationService.delete(anno.getId()));

			// deleting attached picture file
			fileStorage.delete(toDelete.getUrl());

			//delete picture
			photoRepository.delete(toDelete);

		} catch (Exception e) {
			e.printStackTrace();
			throw new TechnicalValidationException("Error while deleting image file");
		}
		return true;
	}

}
