package com.weproov.checkpict.services.impl;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.weproov.checkpict.entites.Annotation;
import com.weproov.checkpict.entites.AnnotationAdditionalPicture;
import com.weproov.checkpict.entites.User;
import com.weproov.checkpict.exceptions.BusinessValidationException;
import com.weproov.checkpict.exceptions.ResourceNotFoundException;
import com.weproov.checkpict.repositories.AnnotationAdditionalPictureRepository;
import com.weproov.checkpict.services.IAnnotationAdditionalPictureService;
import com.weproov.checkpict.services.filestorage.ImageFilesStorageService;

@Service
public class AnnotationAdditionalPictureService implements IAnnotationAdditionalPictureService {

	@Autowired
	private UserService userService;
	
	@Autowired
	private AnnotationAdditionalPictureRepository repository;
	
	@Autowired
	private AnnotationService annotationService;
	
	@Autowired
	private ImageFilesStorageService fileStorage;
	
	@Override
	public Page<AnnotationAdditionalPicture> findAll(int page, int size) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public Page<AnnotationAdditionalPicture> findAll(Long annotationId, int page, int size) {
		return repository.findByAnnotationId(annotationId, getPageable(page, size));
	}

	@Override
	public AnnotationAdditionalPicture find(Long id) {
		return repository.findById(id).orElseThrow(ResourceNotFoundException::new);
	}

	@Override
	public AnnotationAdditionalPicture save(@Valid AnnotationAdditionalPicture entity) {
		return repository.save(entity);
	}
	
	@Transactional
	public AnnotationAdditionalPicture save(Long annotationId, MultipartFile image) {
		Annotation annotation = annotationService.find(annotationId);
		
		if(!annotation.getPhoto().getInspection().getOwner().getId().equals(userService.getConnectedUser().getId()))
			throw new ResourceNotFoundException();
		
		if(image == null)
			throw new BusinessValidationException("Please, provide attached image file.");
		
		AnnotationAdditionalPicture additionalPicture = new AnnotationAdditionalPicture();
		additionalPicture.setUrl(fileStorage.save(image));
		additionalPicture.setAnnotation(annotation);
		
		return save(additionalPicture);
	}

	@Override
	public boolean delete(Long id) {
		User connectedUser = userService.getConnectedUser();
		AnnotationAdditionalPicture additionalPicture = find(id);
		if(!connectedUser.getId().equals(additionalPicture.getAnnotation().getPhoto().getInspection().getOwner().getId()))
			throw new BusinessValidationException("Only inspection's owner can delete annotation");
		try {
			fileStorage.delete(additionalPicture.getUrl());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		repository.delete(additionalPicture);
		return true;
	}

	@Override
	public AnnotationAdditionalPicture update(AnnotationAdditionalPicture entity, Long id) {
		// TODO Auto-generated method stub
		return null;
	}

}
