package com.weproov.checkpict.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.weproov.checkpict.entites.Annotation;
import com.weproov.checkpict.entites.User;
import com.weproov.checkpict.entites.forms.AnnotationForm;
import com.weproov.checkpict.exceptions.BusinessValidationException;
import com.weproov.checkpict.exceptions.ResourceNotFoundException;
import com.weproov.checkpict.repositories.AnnotationRepository;
import com.weproov.checkpict.services.IAnnotationService;

@Service
public class AnnotationService implements IAnnotationService {

	@Autowired
	private UserService userService;
	
	@Autowired
	private AnnotationRepository repository;
	
	@Autowired
	private PhotoService photoService;
	
	@Autowired
	private AnnotationAdditionalPictureService additionalPictureService;
	
	@Override
	public Page<Annotation> findAll(int page, int size) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public Page<Annotation> findAll(Long pictureId, int page, int size) {
		return repository.findByPhotoId(pictureId, getPageable(page, size));
	}

	@Override
	public Annotation find(Long id) {
		return repository.findById(id).orElseThrow(ResourceNotFoundException::new);
	}

	@Override
	public Annotation save(Annotation entity) {
		return repository.save(entity);
	}
	
	public Annotation save(AnnotationForm annotationForm) {
		Annotation annotation = new Annotation();
		annotation.setPhoto(photoService.find(annotationForm.getIdPhoto()));
		annotation.setText(annotationForm.getText());
		annotation.setX(annotationForm.getX());
		annotation.setY(annotationForm.getY());
		return save(annotation);
	}
	
	public Annotation save(AnnotationForm annotationForm, MultipartFile[] images) {
		Annotation annotation = save(annotationForm);
		if(images != null && images.length > 0){
			for(int i = 0; i < images.length; i++) {
				additionalPictureService.save(annotation.getId(), images[i]);
			}
		}
		return annotation;
	}
	

	@Override
	public Annotation update(Annotation entity, Long id) {
		Annotation old = find(id);
		if(!userService.getConnectedUser().getId().equals(old.getPhoto().getInspection().getOwner().getId()))
			throw new ResourceNotFoundException();
		old.setText(entity.getText());
		old.setX(entity.getX());
		old.setY(entity.getY());
		return repository.save(old);
	}

	@Override
	public boolean delete(Long id) {
		User connectedUser = userService.getConnectedUser();
		Annotation annotation = find(id);
		if(!connectedUser.getId().equals(annotation.getPhoto().getInspection().getOwner().getId()))
			throw new BusinessValidationException("Only inspection owner can delete annotation");
		
		// Delete additional pictures (and files)
		if(annotation.getAdditionalPictures() != null) {
			annotation.getAdditionalPictures()
			.stream()
			.forEach(additionalPict -> additionalPictureService.delete(additionalPict.getId()));
		}
		repository.delete(annotation);
		return true;
	}

}
