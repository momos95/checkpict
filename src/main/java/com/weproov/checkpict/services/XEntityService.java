package com.weproov.checkpict.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.weproov.checkpict.entites.common.XEntity;

public interface XEntityService<T extends XEntity> {
	Page<T> findAll(int page, int size) ;
	T find(Long id) ;
	T save(T entity) ;
	T update(T entity, Long id) ;
	boolean delete(Long id) ;
	public default Pageable getPageable(int page, int size) {
		return PageRequest.of(page, size, Sort.by("id").descending());
	}
}
