package com.weproov.checkpict.factories;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;

import com.weproov.checkpict.entites.common.XEntity;
import com.weproov.checkpict.utils.ErrorResponse;

public class ErrorResponseFactory {

	private ErrorResponseFactory(){	
	}
	
	public static ErrorResponse buildErrorResponse(Set<ConstraintViolation<XEntity>> violations){		
		List<String> constarintMsg = new ArrayList<>();
		violations.forEach (constraint -> constarintMsg.add (constraint.getMessage ()));
		return  new ErrorResponse (LocalDateTime.now(), constarintMsg);
	}
	
	public static ErrorResponse buildErrorResponse(String message){
		return new ErrorResponse(LocalDateTime.now(), List.of(message)) ;
	}

}
