package com.weproov.checkpict;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import com.weproov.checkpict.services.filestorage.ImageFilesStorageService;

@SpringBootApplication
public class CheckpictApplication extends SpringBootServletInitializer implements CommandLineRunner {

	@Autowired
	private ImageFilesStorageService filesStorageService;
	
	public static void main(String[] args) {
		SpringApplication.run(CheckpictApplication.class, args);
	}
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(CheckpictApplication.class);
	}

	@Override
	public void run(String... args) throws Exception {
		filesStorageService.init();
	}


}
