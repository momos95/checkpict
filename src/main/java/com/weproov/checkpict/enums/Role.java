package com.weproov.checkpict.enums;

public enum Role {
	/**
	 * Not signed-up Users
	 * Used to create an order without signing up.
	 */
	GUEST,
	
	/**
	 * For authenticated and signed-up Users
	 */
	CUSTOMER,
	
	
	/**
	 * Delivery guy users.
	 */
	DELIVERY_MAN,
	
	/**
	 * For poultry houses owners
	 */
	CHICK_SUPPLIER,
	
	/**
	 * For Administrators
	 */
	ADMIN
}
