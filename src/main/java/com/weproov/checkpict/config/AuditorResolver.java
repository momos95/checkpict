package com.weproov.checkpict.config;

import java.util.Optional;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.weproov.checkpict.entites.User;

@Configuration
@EnableJpaAuditing
public class AuditorResolver implements AuditorAware<User> {

	@Override
	public Optional<User> getCurrentAuditor() {
		try  {
			return Optional.ofNullable(SecurityContextHolder.getContext())
			.map(org.springframework.security.core.context.SecurityContext::getAuthentication)
            .filter(Authentication::isAuthenticated)
            .map(Authentication::getPrincipal)
            .map(User.class::cast);
		}catch (Exception e) {
			return Optional.empty() ;
		}
	            
	}
}
