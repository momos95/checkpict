package com.weproov.checkpict.config;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponents;

@Service
public class SecurityContext {

  public String getLoggedUser(){
    return SecurityContextHolder.getContext().getAuthentication().getName();
  }
  
  public UriComponents getRequestUri(){
    return ServletUriComponentsBuilder.fromCurrentContextPath().build();
  }
}
