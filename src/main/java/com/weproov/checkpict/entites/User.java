package com.weproov.checkpict.entites;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.weproov.checkpict.constants.ValidationMessages;
import com.weproov.checkpict.entites.common.XAuditedEntity;
import com.weproov.checkpict.entites.common.XEntity;
import com.weproov.checkpict.enums.Role;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "TBP_WPV_USER")
@EqualsAndHashCode(callSuper=false)
@JsonIgnoreProperties(value = {"password"}, allowSetters = true)
public class User extends XAuditedEntity implements XEntity {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)	
	private Long id ;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1560930567799396808L;
	
	@NotBlank(message = ValidationMessages.USER_LAST_NAME_NOT_BLANK)
	private String nom ;
	
	@NotBlank(message = ValidationMessages.USER_FIST_NAME_NOT_BLANK)
	private String prenom ;
	
	@Email(message = ValidationMessages.USER_E_MAIL_FORM_VALIDE)
	private String username ;
	
	@NotBlank(message = ValidationMessages.USER_PASSWORD_NOT_BLANK)
	private String password ;
	
	@Enumerated(EnumType.STRING)
	private Role role ;

	private boolean active = true;
}
