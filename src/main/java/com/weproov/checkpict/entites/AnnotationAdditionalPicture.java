package com.weproov.checkpict.entites;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.weproov.checkpict.entites.common.XAuditedEntity;
import com.weproov.checkpict.entites.common.XEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "TBP_WPV_ANNOTATION_ADDITIONAL_PICTURE")
@EqualsAndHashCode(callSuper=false)
@ApiModel(description = "Contains all information related to Annotations additional pictures")
public class AnnotationAdditionalPicture extends XAuditedEntity implements XEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6294739524511925131L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)	
	private Long id ;
	
	@NotBlank
	@ApiModelProperty(value = "Additional Annotation Attached Picture's URL")
	private String url;
	
	@NotNull
	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="annotation_id")
	@ApiModelProperty(value = "Concerned annotation: mandatory (use entityForm with ID)")
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler", "additionalPictures"})
	private Annotation annotation;
	
}
