package com.weproov.checkpict.entites.common;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.weproov.checkpict.entites.User;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class XAuditedEntity implements XEntity {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8428084166678508146L;

	@CreationTimestamp
	protected LocalDateTime createdAt ;
	
	@JsonIgnore
	@UpdateTimestamp
	protected LocalDate updatedAt ;	
	
	@CreatedBy
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "created_by_user_id", updatable = false)
    @JsonIgnore
	protected User createdBy = null;
	
	@LastModifiedBy
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "last_modified_by_user_id")
    @JsonIgnore
	protected User lastModifiedBy ;
}
