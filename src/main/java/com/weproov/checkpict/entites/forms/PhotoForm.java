package com.weproov.checkpict.entites.forms;

import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PhotoForm {
	
	@NotBlank(message = "Veuillez renseigner le titre de la photo.")
	private String title;
	
	@NotNull(message = "Veuiller renseigner l'identifiant de l'inspection concernée.")
	private Long idInspection;
	
	private List<SimpleAnnotationForm> annotations;

}
