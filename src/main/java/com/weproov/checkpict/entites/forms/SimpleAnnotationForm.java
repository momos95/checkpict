package com.weproov.checkpict.entites.forms;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SimpleAnnotationForm {
	
	@NotBlank(message = "Veuillez remplir le corps de l'annotation.")
	protected String text;
	
	@NotNull(message = "Veuillez indiquer l'abscisse du dégat sur la photo.")
	protected double x;
	
	@NotNull(message = "Veuillez indiquer l'ordonnée du dégat sur la photo.")
	protected double y;
	
}
