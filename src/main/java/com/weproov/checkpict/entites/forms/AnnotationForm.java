package com.weproov.checkpict.entites.forms;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class AnnotationForm extends SimpleAnnotationForm {
	
	@NotNull(message = "Veuillez indiquer l'identifiant de la photo concernée.")
	private Long idPhoto;
	
}
