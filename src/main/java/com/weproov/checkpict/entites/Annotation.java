package com.weproov.checkpict.entites;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.weproov.checkpict.entites.common.XAuditedEntity;
import com.weproov.checkpict.entites.common.XEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "TBP_WPV_ANNOTATION")
@EqualsAndHashCode(callSuper=false)
@ApiModel(description = "Contains all information related to picture's Annotations")
public class Annotation extends XAuditedEntity implements XEntity {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4848141468485712034L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)	
	private Long id ;
	
	@NotBlank
	@ApiModelProperty(value = "annotation's description: mandatory")
	private String text;
	
	@NotNull
	@JsonBackReference
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="photo_id")
	@ApiModelProperty(value = "Concerned picture: mandatory (use entityForm with ID)")
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler", "annotations"})
	private Photo photo;
	
	@ApiModelProperty(value = "X - Coordinate: mandatory")
	@NotNull(message = "Veuillez indiquer l'abscisse du dégat sur la photo.")
	private double x;
	
	@ApiModelProperty(value = "Y - Coordinate: mandatory")
	@NotNull(message = "Veuillez indiquer l'ordonnée du dégat sur la photo.")
	private double y;
	
	@JsonIgnoreProperties("annotation")
	@OneToMany(fetch=FetchType.LAZY, mappedBy="annotation")
	@ApiModelProperty(value="annotations additional pictures")
	private List<AnnotationAdditionalPicture> additionalPictures;

}
