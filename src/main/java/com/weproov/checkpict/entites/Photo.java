package com.weproov.checkpict.entites;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.weproov.checkpict.entites.common.XAuditedEntity;
import com.weproov.checkpict.entites.common.XEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "TBP_WPV_PHOTO")
@EqualsAndHashCode(callSuper=false)
@ApiModel(description = "Entity for picture information management")
public class Photo extends XAuditedEntity implements XEntity {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3556070581615328975L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)	
	private Long id ;
	
	@NotBlank
	@ApiModelProperty(value = "Picture title: mandatory")
	private String title;
	
	@ApiModelProperty(value = "Picture file URL")
	private String url;
	
	@NotNull
	@JsonBackReference
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="inspection_id")
	@ApiModelProperty(value = "Actual picture belong to given Inspection")
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler", "photos"})
	private Inspection inspection;
	
	@JsonManagedReference
	@JsonIgnoreProperties("photo")
	@OneToMany(fetch=FetchType.LAZY, mappedBy="photo")
	@ApiModelProperty(value="picture's annotations")
	private List<Annotation> annotations;

}
