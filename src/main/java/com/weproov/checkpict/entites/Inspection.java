package com.weproov.checkpict.entites;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.weproov.checkpict.entites.common.XAuditedEntity;
import com.weproov.checkpict.entites.common.XEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "TBP_WPV_INSPECTION")
@EqualsAndHashCode(callSuper=false)
@ApiModel(description = "Inspections Entity: containing all information related to inspections")
public class Inspection extends XAuditedEntity implements XEntity {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8161467081516005861L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)	
	private Long id ;
	
	@NotBlank
	private String title;
	
	@JoinColumn(name="owner_id")
	@ManyToOne(fetch = FetchType.LAZY)
	@ApiModelProperty(value = "Inspection's owner")
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private User owner;
	
	@JsonManagedReference
	@JsonIgnoreProperties("inspection")
	@ApiModelProperty(value = "Inspection's attached pictures")
	@OneToMany(fetch=FetchType.LAZY, mappedBy="inspection")
	private List<Photo> photos;

}
