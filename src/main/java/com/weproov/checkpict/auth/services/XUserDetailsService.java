package com.weproov.checkpict.auth.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.weproov.checkpict.entites.User;
import com.weproov.checkpict.enums.Role;
import com.weproov.checkpict.repositories.UserRepository;

@Service
public class XUserDetailsService implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;


	private List<GrantedAuthority> getPrivileges(Role role) {
		SimpleGrantedAuthority simpleGrantedAuthority =
				new SimpleGrantedAuthority(role.name());
		return List.of(simpleGrantedAuthority);

	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepository.findByUsername(username)
				.orElseThrow(() -> new UsernameNotFoundException(String.format("User Not Found with -> identifiant : %s", username)));
		List<GrantedAuthority> grantedAuthorities = getPrivileges(user.getRole());
		return new org.springframework.security.core.userdetails.User(
				user.getUsername(), user.getPassword(), grantedAuthorities);
	}

}
