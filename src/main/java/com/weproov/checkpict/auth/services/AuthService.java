package com.weproov.checkpict.auth.services;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import com.weproov.checkpict.auth.JwtProvider;
import com.weproov.checkpict.auth.forms.JwtResponse;
import com.weproov.checkpict.auth.forms.SignIn;
import com.weproov.checkpict.auth.forms.SignupForm;
import com.weproov.checkpict.constants.ExceptionMessages;
import com.weproov.checkpict.entites.User;
import com.weproov.checkpict.enums.Role;
import com.weproov.checkpict.exceptions.ResourceAlreadyExistsException;
import com.weproov.checkpict.exceptions.UserBadCredentialsException;
import com.weproov.checkpict.repositories.UserRepository;

@Service
public class AuthService {
	
	private Logger logger = LoggerFactory.getLogger(AuthService.class);

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	JwtProvider jwtProvider;

	@Autowired
	UserRepository userRepository;

	@Autowired
	PasswordEncoder encoder;

	public ResponseEntity<JwtResponse> refreshToken(String token){
		String username = jwtProvider.getUserNameFromJwtToken(token) ;
		if( username != null && userRepository.existsByUsername(username)) {
			return ResponseEntity.ok(new JwtResponse(jwtProvider.generateJwtToken(username),username,null)) ;
		}
		throw new  AccessDeniedException(ExceptionMessages.ACCESS_DENIED) ;
	}

	public ResponseEntity<JwtResponse> authenSignInResponse(
			@RequestBody @Valid SignIn signIn) throws AuthenticationException{
		@NotBlank @Size(min = 3, max = 60) String username = signIn.getUsername().trim();
		@NotBlank @Size(min = 2, max = 40) String password = signIn.getPassword().trim();
		try {
			Authentication authentication = authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(
							username, password)
					);
			SecurityContextHolder.getContext().setAuthentication(authentication);
			String jwt = jwtProvider.generateJwtToken(authentication);
			UserDetails userDetails = (UserDetails) authentication.getPrincipal();
			return ResponseEntity.ok(
					new JwtResponse(jwt, userDetails.getUsername(), userDetails.getAuthorities()));
		}catch (AuthenticationException authenticationException){
			logger.error("{0}", authenticationException);
			throw new UserBadCredentialsException();
		}
	}

	public ResponseEntity<JwtResponse> authenSignUpResponse(
			@RequestBody @Valid SignupForm signUp) {
		@NotBlank @Size(max = 50) String username = signUp.getUsername().trim();
		String password = signUp.getPassword();
		if (userRepository.existsByUsername(username)) {
			logger.error("Fail -> Email {} is already taken!", username);
			throw new ResourceAlreadyExistsException(String.format("Fail -> Email {} is already taken!", username));
		}
		
		User user = new User() ;
		user.setNom(signUp.getNom().trim());
		user.setPrenom(signUp.getPrenom().trim());
		user.setUsername(signUp.getUsername().trim());
		user.setRole(Role.CUSTOMER); 
		user.setPassword(encoder.encode(password));
		user.setActive(true); 
		userRepository.save(user);
		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(
						user.getUsername(),
						password
						)
				);
		String jwt = jwtProvider.generateJwtToken(authentication);
		return new ResponseEntity<JwtResponse>(new JwtResponse(jwt, user.getUsername(), authentication.getAuthorities()), HttpStatus.CREATED);

	}
}
