package com.weproov.checkpict.auth;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import com.weproov.checkpict.auth.services.XUserDetailsService;

import io.jsonwebtoken.ExpiredJwtException;

public class JwtAuthTokenFilter extends OncePerRequestFilter {

  private static final Logger APP_LOGGER = LoggerFactory.getLogger(JwtAuthTokenFilter.class);
  @Autowired
  private JwtProvider tokenProvider;
  @Autowired
  private XUserDetailsService customUserDetailsService;

  @Override
  protected void doFilterInternal(HttpServletRequest request,
                                  HttpServletResponse response,
                                  FilterChain filterChain)
    throws ServletException, IOException {
    try {

      String jwtToken = getJwt(request);
      if (jwtToken != null && tokenProvider.validateJwtToken(jwtToken)) {
        String username = tokenProvider.getUserNameFromJwtToken(jwtToken);

        UserDetails userDetails = customUserDetailsService.loadUserByUsername(username);
        UsernamePasswordAuthenticationToken authentication
          = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
        authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String newToken = tokenProvider.generateJwtToken(authentication);
        response.setHeader("Authorization", newToken);
      }
    }
    catch (ExpiredJwtException ex) {
		
		String requestURL = request.getRequestURL().toString();
		if (requestURL.contains("refreshToken")) {
			allowForRefreshToken(ex, request);
		} else
			request.setAttribute("exception", ex);

	} catch (Exception e) {
      APP_LOGGER.error(String.format("Can NOT set user authentication -> Message: %s", e.getMessage()));
    }

    filterChain.doFilter(request, response);
  }

  private String getJwt(HttpServletRequest request) {
    String authHeader = request.getHeader("Authorization");

    if (authHeader != null && authHeader.startsWith("Bearer ")) {
      return authHeader.replace("Bearer ", "");
    }

    return null;
  }
  
  private void allowForRefreshToken(ExpiredJwtException ex, HttpServletRequest request) {

		UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
				null, null, null);
		SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
		request.setAttribute("claims", ex.getClaims());

	}
  
}
