package com.weproov.checkpict.auth.forms;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import com.weproov.checkpict.constants.ValidationMessages;
import com.weproov.checkpict.enums.Role;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SignupForm {

	@NotBlank(message = ValidationMessages.USER_LAST_NAME_NOT_BLANK)
	protected String nom ;
	
	@NotBlank(message = ValidationMessages.USER_FIST_NAME_NOT_BLANK)
	protected String prenom ;
	
	@Email(message = ValidationMessages.USER_E_MAIL_FORM_VALIDE)
	private String username ;
	
	@NotBlank(message = ValidationMessages.USER_PASSWORD_NOT_BLANK)
	private String password ;
	
	@Enumerated(EnumType.STRING)
	private Role role ;
}
