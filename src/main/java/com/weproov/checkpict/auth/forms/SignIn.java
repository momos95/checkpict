package com.weproov.checkpict.auth.forms;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class SignIn {
  @NotBlank(message = "Veuillez saisir l'identifiant (adresse mail ou numéro de téléphone)")
  @Size(min = 8, message = "L'identifiant doit contenir au moins 8 caractères.")
  private String username;

  @NotBlank
  @Size(min = 8, max = 20, message = "Le mot de passe doit être composé de 8 à 20 caractères.")
  private String password;
}
