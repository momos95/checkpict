package com.weproov.checkpict.constants;

public class PageConstant {
	
    public static final String DEFAULT_PAGE="0";
    public static final String DEFAULT_SIZE="20";
    
    private PageConstant() { // NOSONAR
    	
    }

}
