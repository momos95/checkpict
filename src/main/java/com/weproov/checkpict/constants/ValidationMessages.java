package com.weproov.checkpict.constants;

public class ValidationMessages {
	
	
	/***************************************************** User ******************************************************/
	public static final String USER_LAST_NAME_NOT_BLANK 			= "Veuillez saisir le champs 'nom'." ;
	public static final String USER_FIST_NAME_NOT_BLANK 			= "Veuillez saisir le champs 'prenom'." ;
	public static final String USER_E_MAIL_NOT_BLANK 				= "Veuillez saisir une adresse e-mail." ;
	public static final String USER_E_MAIL_IS_UNIQUE 				= "L'adresse E-mail saisie est déjà prise." ;
	public static final String USER_E_MAIL_FORM_VALIDE 				= "Le format de l'adresse e-mail saisie est incorrect." ;
	public static final String USER_PASSWORD_NOT_BLANK 				= "Veuillez saisir un mot de passe." ;
	public static final String USER_GENDER_NOT_BLANK 				= "Veuillez remplir le champs 'Genre'." ;
	
	private ValidationMessages() { //NOSONAR
		
	}
	
}