package com.weproov.checkpict.constants;

public class ExceptionMessages {
	
	/******************************************* GENERIC *******************************************/
	public static final String RESOURCE_ALREADY_EXISTS 	= "The resource you're trying to add already exists." ;
	public static final String RESOURCE_NOT_FOUND 		= "La resource que vous recherchez est introuvable." ;
	public static final String ACCESS_DENIED			= "Vous ne disposez pas des droits d'accès nécessaires." ;
	
	private ExceptionMessages() { // NOSONAR
		
	}
}
