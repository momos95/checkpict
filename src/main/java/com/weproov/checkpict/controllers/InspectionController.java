package com.weproov.checkpict.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.weproov.checkpict.constants.PageConstant;
import com.weproov.checkpict.entites.Inspection;
import com.weproov.checkpict.services.impl.InspectionService;

import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

@RestController
@RequestMapping("/app/rest/api/inspections")
@Api(value = "Manage all inspections operations")
public class InspectionController {
	
	final Logger logger = LoggerFactory.getLogger(InspectionController.class);
	
	@Autowired
	private InspectionService inspectionService;
	
	@GetMapping("")
	@Operation(summary = "Retrieve all known inspections", responses = {
			@ApiResponse(responseCode = "200", 
					description = "Paginated inspections (ordered descendly by ID)", 
					content=@Content(mediaType = "application/json"))
	})
	public ResponseEntity<Page<Inspection>> get(
			@RequestParam(defaultValue = PageConstant.DEFAULT_PAGE) int page,
			@RequestParam(defaultValue = PageConstant.DEFAULT_SIZE) int size){
		logger.info("Getting all inspections");
		return ResponseEntity.ok(inspectionService.findAll(page, size));
	}
	
	
	@GetMapping("/{id}")
	@Operation(summary = "Retrieve one inspection", responses = {
			@ApiResponse(responseCode = "200", 
					description = "Corresponding inspection to given ID", 
					content=@Content(mediaType = "application/json"))
	})
	public ResponseEntity<Inspection> get(@PathVariable Long id){
		logger.info("Finding corresponding inspection to given id " + id);
		return ResponseEntity.ok(inspectionService.find(id));
	}
	
	@PostMapping("")
	@Operation(summary = "Create new empty inspection", responses = {
			 @ApiResponse(responseCode = "201", 
					 description = "new successfully created inspection", 
					 content = @Content(mediaType = "application/json"))
	})
	public ResponseEntity<Inspection> create(@RequestBody Inspection inspection){
		logger.info("creating new empty inspection");
		return new ResponseEntity<Inspection>(inspectionService.save(inspection), HttpStatus.CREATED);
	}
	
	
	@DeleteMapping("/{id}")
	@Operation(summary = "Delete an inspection", responses = {
			@ApiResponse(responseCode = "200", 
					description = "Inspection corresponding to given ID is deleted")
	})
	public ResponseEntity<Boolean> delete(@PathVariable Long id){
		logger.info("Deleting corresponding inspection to given ID :"+id);
		return ResponseEntity.ok(inspectionService.delete(id));
	}

}
