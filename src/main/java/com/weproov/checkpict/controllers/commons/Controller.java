package com.weproov.checkpict.controllers.commons;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import com.weproov.checkpict.exceptions.TechnicalValidationException;

public interface Controller {

	default void businessValidation(Validator validator, Object entryData) {
		Set<ConstraintViolation<Object>> violations = validator.validate(entryData);
		if(!violations.isEmpty()) {
			StringBuilder message = new StringBuilder();
			violations.forEach (constraint -> message.append(constraint.getMessage ()));
			throw new TechnicalValidationException(message.toString()) ;
		}
	}
}
