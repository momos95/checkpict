package com.weproov.checkpict.controllers;

import javax.validation.Validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.weproov.checkpict.constants.PageConstant;
import com.weproov.checkpict.controllers.commons.Controller;
import com.weproov.checkpict.entites.Photo;
import com.weproov.checkpict.entites.forms.PhotoForm;
import com.weproov.checkpict.services.impl.PhotoService;
import com.weproov.checkpict.utils.EntityStringJsonParser;

import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

@RestController
@RequestMapping("/app/rest/api/pictures")
@Api(value = "inspections pictures management")
public class PhotoController implements Controller {

	final Logger logger = LoggerFactory.getLogger(PhotoController.class);
	
	 /**
     * Checker used to validate entry bean.
     */
    @Autowired
    private Validator validator;

	@Autowired
	private PhotoService photoService;
	
	@GetMapping("")
	@Operation(summary = "Retrieve inspection's pictures by ID", responses = {
			@ApiResponse(responseCode = "200", 
					description = "Paginated results", 
					content=@Content(mediaType = "application/json"))
	})
	public ResponseEntity<Page<Photo>> get(
			@RequestParam(defaultValue = PageConstant.DEFAULT_PAGE) int page,
			@RequestParam(defaultValue = PageConstant.DEFAULT_SIZE) int size,
			@RequestParam Long inspectionId){
		return ResponseEntity.ok(photoService.findAll(inspectionId, page, size));
	}

	@GetMapping("/{id}")
	@Operation(summary = "Retrieve one picture by ID", responses = {
			@ApiResponse(responseCode = "200", 
					description = "Corresponding picture to given ID", 
					content=@Content(mediaType = "application/json"))
	})
	public ResponseEntity<Photo> get(@PathVariable Long id){
		return ResponseEntity.ok(photoService.find(id));
	}

	@PostMapping("")
	@Operation(summary = "Add picture to an inspection", responses = {
			 @ApiResponse(responseCode = "201", 
					 description = "new successfully added picture", 
					 content = @Content(mediaType = "application/json"))
	})
	public ResponseEntity<Photo> create(
			@RequestPart(name = "picture") String photoFormStr,
			@RequestPart MultipartFile image){
		PhotoForm photoForm = EntityStringJsonParser.getPhotoFromString(photoFormStr);
		businessValidation(validator, photoForm);
		return new ResponseEntity<Photo>(photoService.save(photoForm, image), HttpStatus.CREATED);

	}

	@DeleteMapping("/{id}")
	@Operation(summary = "Delete a picture and all attached annotations", responses = {
			@ApiResponse(responseCode = "200", 
					description = "Picture corresponding to given ID is deleted")
	})
	public ResponseEntity<Boolean> delete(@PathVariable Long id){
		logger.info("Deleting corresponding picture to given ID :"+id);
		return ResponseEntity.ok(photoService.delete(id));
	}
}
