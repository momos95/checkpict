package com.weproov.checkpict.controllers;

import javax.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.weproov.checkpict.constants.PageConstant;
import com.weproov.checkpict.controllers.commons.Controller;
import com.weproov.checkpict.entites.Annotation;
import com.weproov.checkpict.entites.forms.AnnotationForm;
import com.weproov.checkpict.services.impl.AnnotationService;
import com.weproov.checkpict.utils.EntityStringJsonParser;

import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

@RestController
@RequestMapping("/app/rest/api/annotations")
@Api(value = "Pictures Annotations management")
public class AnnotationController implements Controller {
	
	@Autowired
	private AnnotationService annotationService;
	
	@GetMapping("")
	@Operation(summary = "Retrieve picture's annotations by ID", responses = {
			@ApiResponse(responseCode = "200", 
					description = "Paginated results", 
					content=@Content(mediaType = "application/json"))
	})
	public ResponseEntity<Page<Annotation>> get(
			@RequestParam(defaultValue = PageConstant.DEFAULT_PAGE) int page,
			@RequestParam(defaultValue = PageConstant.DEFAULT_SIZE) int size,
			@RequestParam Long pictureId){
		return ResponseEntity.ok(annotationService.findAll(pictureId, page, size));
	}
	
	@Autowired
	private Validator validator;
	
	@GetMapping("/{id}")
	@Operation(summary = "Retrieve an annotation by ID", responses = {
			@ApiResponse(responseCode = "200", 
					description = "Corresponding annotation to given ID", 
					content=@Content(mediaType = "application/json"))
	})
	public ResponseEntity<Annotation> get(@PathVariable Long id){
		return ResponseEntity.ok(annotationService.find(id));
	}
	
	@PostMapping("")
	@Operation(summary = "Add annotation to a picture", responses = {
			 @ApiResponse(responseCode = "201", 
					 description = "new successfully added annotation", 
					 content = @Content(mediaType = "application/json"))
	})
	public ResponseEntity<Annotation> create(
			@RequestPart(name = "annotation") String annotationFormStr,
			@RequestPart(required = false) MultipartFile[] images){
		AnnotationForm annotationForm = EntityStringJsonParser.getAnnotationFromString(annotationFormStr);
		businessValidation(validator, annotationForm);
		return new ResponseEntity<Annotation>(annotationService.save(annotationForm, images), HttpStatus.CREATED);

	}
	
	@DeleteMapping("/{id}")
	@Operation(summary = "Delete an anotation and all attached additional pictures", responses = {
			@ApiResponse(responseCode = "200", 
					description = "Annotation corresponding to given ID is deleted")
	})
	public ResponseEntity<Boolean> delete(@PathVariable Long id){
		return ResponseEntity.ok(annotationService.delete(id));
	}

}
