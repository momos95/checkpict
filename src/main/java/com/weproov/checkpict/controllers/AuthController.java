package com.weproov.checkpict.controllers;

import javax.validation.Validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.weproov.checkpict.auth.forms.JwtResponse;
import com.weproov.checkpict.auth.forms.SignIn;
import com.weproov.checkpict.auth.forms.SignupForm;
import com.weproov.checkpict.controllers.commons.Controller;

@RestController
@RequestMapping("/app/rest/api/auth")
public class AuthController implements Controller {

	Logger logger = LoggerFactory.getLogger(AuthController.class);

	@Autowired
	private Validator validator;


	@Autowired
	private com.weproov.checkpict.auth.services.AuthService authService;

	@PostMapping("/signin")
	public ResponseEntity<JwtResponse> signIn(@RequestBody SignIn signIn) {
		businessValidation(validator, signIn);
		return authService.authenSignInResponse(signIn);
	}

	@GetMapping("/refreshToken/{refreshToken}")
	public ResponseEntity<JwtResponse> refreshToken(@PathVariable String refreshToken){
		return authService.refreshToken(refreshToken);  
	}

	@PostMapping("/signup")
	public ResponseEntity<JwtResponse> signUp(@RequestBody SignupForm signUp) {
		businessValidation(validator, signUp);
 		return authService.authenSignUpResponse(signUp);
	}
}
