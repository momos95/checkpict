package com.weproov.checkpict.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.weproov.checkpict.constants.PageConstant;
import com.weproov.checkpict.entites.AnnotationAdditionalPicture;
import com.weproov.checkpict.services.impl.AnnotationAdditionalPictureService;

import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

@RestController
@RequestMapping("/app/rest/api/additional-pictures")
@Api(value = "Annotations additional pictures management")
public class AdditionalPictureController {
	
	@Autowired
	private AnnotationAdditionalPictureService additionalPictureService;
	
	
	@GetMapping("")
	@Operation(summary = "Retrieve an annotation's additional picture by ID", responses = {
			@ApiResponse(responseCode = "200", 
					description = "Paginated results", 
					content=@Content(mediaType = "application/json"))
	})
	public ResponseEntity<Page<AnnotationAdditionalPicture>> get(
			@RequestParam(defaultValue = PageConstant.DEFAULT_PAGE) int page,
			@RequestParam(defaultValue = PageConstant.DEFAULT_SIZE) int size,
			@RequestParam Long annotationId){
		return ResponseEntity.ok(additionalPictureService.findAll(annotationId, page, size));
	}
	
	
	@PostMapping("")
	@Operation(summary = "Add an additional picture to an annotation", responses = {
			 @ApiResponse(responseCode = "201", 
					 description = "new successfully added picture", 
					 content = @Content(mediaType = "application/json"))
	})
	public ResponseEntity<AnnotationAdditionalPicture> create(
			@RequestPart String annotationId,
			@RequestPart MultipartFile picture){
		return new ResponseEntity<AnnotationAdditionalPicture>(additionalPictureService.save(Long.valueOf(annotationId), picture), HttpStatus.CREATED);
	}
	
	@GetMapping("/{id}")
	@Operation(summary = "Retrieve an annotation's additional picture by ID", responses = {
			@ApiResponse(responseCode = "200", 
					description = "Corresponding annotation's additional picture to given ID", 
					content=@Content(mediaType = "application/json"))
	})
	public ResponseEntity<AnnotationAdditionalPicture> get(@PathVariable Long id){
		return ResponseEntity.ok(additionalPictureService.find(id));
	}
	
	@DeleteMapping("/{id}")
	@Operation(summary = "Delete an annotation's additional picture", responses = {
			@ApiResponse(responseCode = "200", 
					description = "Additional Picture corresponding to given ID is deleted")
	})
	public ResponseEntity<Boolean> delete(@PathVariable Long id){
		return ResponseEntity.ok(additionalPictureService.delete(id));
	}

}
