package com.weproov.checkpict.controllers.advices;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.fasterxml.jackson.core.JsonParseException;
import com.weproov.checkpict.exceptions.BusinessValidationException;
import com.weproov.checkpict.exceptions.ResourceAlreadyExistsException;
import com.weproov.checkpict.exceptions.ResourceNotFoundException;
import com.weproov.checkpict.exceptions.TechnicalValidationException;
import com.weproov.checkpict.exceptions.UserBadCredentialsException;
import com.weproov.checkpict.factories.ErrorResponseFactory;
import com.weproov.checkpict.utils.ErrorResponse;


@ControllerAdvice
public class ApplicationControllerAdvice {
	
	@ExceptionHandler(JsonParseException.class)
	public final ResponseEntity<ErrorResponse> handleBadFormatJson(JsonParseException ex){
		return new ResponseEntity<>(ErrorResponseFactory.buildErrorResponse("Please check that the JSON Format is correct"), HttpStatus.BAD_REQUEST) ;
	}
	
	@ExceptionHandler(ResourceNotFoundException.class)
	public final ResponseEntity<ErrorResponse> handleRuntimeException(ResourceNotFoundException ex){
		return new ResponseEntity<>(ErrorResponseFactory.buildErrorResponse(ex.getMessage()), HttpStatus.NOT_FOUND) ;
	}
	
	@ExceptionHandler(ResourceAlreadyExistsException.class)
	public final ResponseEntity<ErrorResponse> handleRuntimeException(ResourceAlreadyExistsException ex){
		return new ResponseEntity<>(ErrorResponseFactory.buildErrorResponse(ex.getMessage()), HttpStatus.BAD_REQUEST) ;
	}
	
	@ExceptionHandler(UserBadCredentialsException.class)
	public final ResponseEntity<ErrorResponse> handleBadCredentialsResponse(UserBadCredentialsException ex){
		return new ResponseEntity<>(ErrorResponseFactory.buildErrorResponse(ex.getMessage()), HttpStatus.UNAUTHORIZED) ;
	}
	
	@ExceptionHandler(TechnicalValidationException.class)
	public final ResponseEntity<ErrorResponse> handleRuntimeException(TechnicalValidationException ex){
		return new ResponseEntity<>(ErrorResponseFactory.buildErrorResponse(ex.getMessage()), HttpStatus.BAD_REQUEST) ;
	}
	
	@ExceptionHandler(BusinessValidationException.class)
	public final ResponseEntity<ErrorResponse> handleRuntimeException(BusinessValidationException ex){
		return new ResponseEntity<>(ErrorResponseFactory.buildErrorResponse(ex.getMessage()), HttpStatus.BAD_REQUEST) ;
	}
}


