package com.weproov.checkpict.utils;

import com.google.gson.Gson;
import com.weproov.checkpict.entites.forms.AnnotationForm;
import com.weproov.checkpict.entites.forms.PhotoForm;

public class EntityStringJsonParser {
	
	public static PhotoForm getPhotoFromString(String str) {
		if(str != null && !str.isBlank()) {
			Gson g = new Gson() ;
			return g.fromJson(str, PhotoForm.class) ;
		}
		return null ;
	}
	
	public static AnnotationForm getAnnotationFromString(String str) {
		if(str != null && !str.isBlank()) {
			Gson g = new Gson() ;
			return g.fromJson(str, AnnotationForm.class) ;
		}
		return null ;
	}
}
