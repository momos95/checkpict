package com.weproov.checkpict.utils;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.slf4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.weproov.checkpict.entites.common.XEntity;
import com.weproov.checkpict.exceptions.TechnicalValidationException;
import com.weproov.checkpict.services.XEntityService;


public class XResponseEntityUtils {
	
	private XResponseEntityUtils() { // NOSONAR
		
	}

	public static final <T extends XEntity> ResponseEntity<SimpleApiPrettyResponse> created(XEntityService<T> service,
			T entity, Logger logger, Validator validator) {

		Set<ConstraintViolation<XEntity>> violations = validator.validate(entity);
		SimpleApiPrettyResponse response = new SimpleApiPrettyResponse();
		if (violations.isEmpty()) {
			response.setEntity(service.save(entity));
			response.setStatus(HttpStatus.CREATED);
			logger.info("{} is successfully created. {}",entity.getClass().getName(), response.getEntity());
		} else {
			StringBuilder message = new StringBuilder();
    		violations.forEach (constraint -> message.append(constraint.getMessage ()));
			logger.error("{} is not created due to : {}",entity.getClass().getName(), message.toString());
    		throw new TechnicalValidationException(message.toString()) ;
		}

		return new ResponseEntity<>(response, response.getStatus());
	}

	public static final <T extends XEntity> ResponseEntity<SimpleApiPrettyResponse> updated(XEntityService<T> service,
			T entity, Long id, Logger logger, Validator validator) {

		Set<ConstraintViolation<XEntity>> violations = validator.validate(entity);
		SimpleApiPrettyResponse response = new SimpleApiPrettyResponse();

		if (violations.isEmpty()) {
			response.setEntity(service.update(entity, id));
			response.setStatus(HttpStatus.OK);
			logger.info("{} is successfully created. {}",entity.getClass().getName(), response.getEntity());
		} else {
			StringBuilder message = new StringBuilder();
    		violations.forEach (constraint -> message.append(constraint.getMessage ()));
			logger.error("{} is not created due to : {}",entity.getClass().getName(), message.toString());
    		throw new TechnicalValidationException(message.toString()) ;
		}

		return new ResponseEntity<>(response, response.getStatus());
	}

	
	
	

}
