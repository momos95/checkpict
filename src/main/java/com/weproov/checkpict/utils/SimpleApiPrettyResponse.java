package com.weproov.checkpict.utils;

import java.time.LocalDateTime;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.weproov.checkpict.entites.common.XEntity;

import lombok.Data;

/**
 * 
 * @author Mamadou
 * General Bean for controller returns. 
 * It contains entity returned by repositories actions and error or warning.
 */
@Data
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class SimpleApiPrettyResponse {

	private XEntity entity ;
	private LocalDateTime timestamp ;
	private HttpStatus status ;
	private ErrorResponse error ;	
	
	public SimpleApiPrettyResponse(){
		timestamp = LocalDateTime.now() ;
		status = HttpStatus.OK ;
	}
	
	public SimpleApiPrettyResponse(ErrorResponse error){
		this() ;
		this.error = error ;
	}
	
	public SimpleApiPrettyResponse(XEntity entity){
		this() ;
		this.entity = entity ;
	}

}
