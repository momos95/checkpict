package com.weproov.checkpict.utils;

import java.time.LocalDateTime;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ErrorResponse {
	
	/*
	 * When it happens exactly
	 */
    private LocalDateTime date;
    
    /**
     * List of error messages
     */
    private List<String> messages;

}
